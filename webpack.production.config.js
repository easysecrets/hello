var webpack = require('webpack');

var config = require('./webpack.config');

var plugins = Array.isArray(config.plugins) ? config.plugins : [];

module.exports = Object.assign({}, config, {
  plugins: plugins.concat([
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.DedupePlugin(),
  ]),
});
