import { ExpressAddon, Addon } from 'bitbucket-connect';

import { installed, uninstalled, helloWorld, createFileViewHandler } from './routes';
import { validateJwtToken, exposeBaseUrl, attachHttpClientFactory } from './middleware';

const addon = new ExpressAddon({
  key: 'hello',
  name: 'hello',
  description: 'Hello World Repository Panel',
  vendor: {
    name: 'Andriy Zhdnaov',
    url: 'https://azhdanov.bitbucket.io/',
  },
  baseUrl: process.env.CONNECT_BASE_URL,
}, {
  middleware: [
    validateJwtToken,
    exposeBaseUrl,
    attachHttpClientFactory,
  ],
});

addon.registerContexts(['account']);
addon.registerScopes(['account', 'email', 'repository']);

if (process.env.CONNECT_OAUTH_CONSUMER_KEY) {
  addon.registerOauthConsumer({ clientId: process.env.CONNECT_OAUTH_CONSUMER_KEY });
}

addon.registerLifecycleRoute('/installed', Addon.Lifecycles.INSTALLED, installed);
addon.registerLifecycleRoute('/uninstalled', Addon.Lifecycles.UNINSTALLED, uninstalled);

addon.registerWebPanelRoute('/hello-world', {
  location: Addon.WebPanelLocations.REPOSITORY_OVERVIEW_INFORMATION_PANEL,
}, helloWorld);

addon.registerFileViewsRoute('/fileView?repoUuid={repo_uuid}&fileCset={file_cset}&filePath={file_path}', {
  name: {value: 'test'},
  file_matches: {value: '*.*'}
}, createFileViewHandler(addon));

export default addon;
