import bodyParser from 'body-parser';
import express from 'express';
import morgan from 'morgan';
import path from 'path';

import addon from './addon';
import { webpackify } from './utils';
import webpackConfig from '../webpack.config';

const app = express();

app.set('view engine', 'jade');
app.set('views', path.join(__dirname, 'views'));
app.use(morgan('combined'));
app.use(bodyParser.json());

if (process.env.NODE_ENV !== 'production') {
  webpackify(app, webpackConfig);
} else {
  app.use('/static', express.static(path.join(__dirname, '..', 'dist')));
}

app.use(addon.routerBaseUrl, addon.router);

const server = app.listen(3000, () => {
  const host = server.address().address === '::' ? 'localhost' : server.address().address;
  const port = server.address().port;

  console.log('hello', `listening at http://${host}:${port}`); // eslint-disable-line no-console
});

export default app;
