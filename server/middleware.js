import { HttpClient, middleware as connectMiddleware } from 'bitbucket-connect';

import { Connection } from './models';

export const validateJwtToken = connectMiddleware.validateJwtToken(clientKey => (
  new Promise((resolve, reject) => {
    Connection.forge({ clientKey }).fetch().then(conn => {
      resolve(conn.get('sharedSecret'));
    }).catch(() => {
      reject(new Error('Error retrieving connection'));
    });
  }
)));

export function exposeBaseUrl(req, res, next) {
  const { clientKey } = req;

  if (!clientKey) {
    next();
    return;
  }

  Connection.forge({ clientKey }).fetch().then(conn => {
    res.locals.baseUrl = conn.get('baseUrl'); // eslint-disable-line no-param-reassign
    next();
  });
}

export function attachHttpClientFactory(req, res, next) {
  const { clientKey } = req;

  if (!clientKey) {
    next();
    return;
  }

  Connection.forge({ clientKey }).fetch().then(conn => {
    req.createHttpClient = addon => new HttpClient(addon, { // eslint-disable-line no-param-reassign
      clientKey,
      sharedSecret: conn.get('sharedSecret'),
      baseUrl: conn.get('baseUrl'),
    });
    next();
  });
}
