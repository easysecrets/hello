import omit from 'lodash.omit';

import { Connection } from './models';

function stringifyLifecyclePayload(payload) {
  return Object.assign({}, payload, {
    user: JSON.stringify(payload.user),
    principal: JSON.stringify(payload.principal),
    consumer: JSON.stringify(payload.consumer),
  });
}

export function installed(req, res) {
  const { clientKey } = req.body;
  const connection = omit(stringifyLifecyclePayload(req.body), 'eventType');

  Connection.forge({ clientKey }).destroy().then(() => {
    Connection.forge(connection).save(null, { method: 'insert' }).then(() => {
      res.status(204).send();
    }).catch(() => {
      res.status(500).send('Unable to save connection');
    });
  });
}

export function uninstalled(req, res) {
  const { clientKey } = req.body;

  Connection.forge({ clientKey }).destroy().then(() => {
    res.status(204).send();
  }).catch(() => {
    res.status(500).send('Unable to clean up connection');
  });
}

export function helloWorld(req, res) {
  res.render('hello', { message: 'Hello, ' });
}

export function createFileViewHandler(addon) {
  return (req, res) => {
    const httpClient = req.createHttpClient(addon);
    const { repoUuid, fileCset, filePath } = req.query;
    const url = `/api/1.0/repositories/{}/${repoUuid}/raw/${fileCset}/${filePath}`;
    httpClient.get(url).then(apiRes => {
      const content = 'NO DATA'; //transformRawSource(apiRes.data);
      res.render('base', { content });
    });
  };
}
